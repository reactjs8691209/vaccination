import React from "react";
import "../css/Header.css";
import short from "../images/rutgon.png";
import { useState } from "react";


export default function Header(){
    const [curPage, setCurPage] = useState('Home');
    const content = ['Home','Services','Schedule','Contact us'];
    return (
    <div id="header">
        <div className="header-ct1">
            <div href="#">Vaccination.ng</div>
            <div className="borderN"></div>
        </div>
        <div className="header-ct2">
            <div className="navbar-collapse">
                <ul className="navbar-nav">
                   {content.map((item,index) => 
                     (<li className="nav-item" key={index}>
                    <a className={"nav-link" + curPage===item?" bold":""} href={'#' + item.toLowerCase()} onClick={()=>{setCurPage(item)}}>{item}</a>
                    {(curPage === item) &&
                        <div>.</div>
                    }
                </li>)
                
                   )}
                </ul>
            </div>
        </div>
        <div className="checkStatus">
            Check Status
        </div>
        <div className="shortNav">
            <img src={short} alt=""/>
        </div>
    </div>
);   
}