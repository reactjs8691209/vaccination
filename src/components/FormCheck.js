import React from "react";
import "../css/FormCheck.css";



export default function FormCheck(){
    
    return (
    <div id="form">
        <div className="form-title">Check your COVID-19 result on our Database</div>
        <div className="form-input">
            <input type="text" autoFocus placeholder="Okeowo"/>
            <input type="text" placeholder="NIK Number"/>
            <button>Check</button>
        </div>
        <div className="form-info">
            Need a certificate for your COVID-19 result? Please click here
        </div>
    </div>
);   
}