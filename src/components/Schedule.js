import React from "react";
import "../css/Schedule.css";
import clock from '../images/clock.png';
import address from '../images/diachi.png';
import calender from '../images/lich.png';
import shield from '../images/khien.png';
export default function Schedule(){
    return (
        <div className="schedule">
            <div className="schedule-title">
                <img src={clock} alt=""/>
                <div>Schedule the vaccination</div>
            </div>
            <div className="schedule-content">
                <div className="schedule-part">
                    <img src={address} alt=""/>
                    <div>
                        <div className="scd-part-title">Location</div>
                        <div className="scd-part-content">Ikeja Lagos, Nigeria</div>
                    </div>
                </div>
                <div className="schedule-part">
                <img src={calender} alt=""/>
                    <div>
                        <div className="scd-part-title">Date</div>
                        <div className="scd-part-content">29 February, 2022</div>
                    </div>
                </div>
                <div className="schedule-part">
                <img src={shield} alt=""/>
                    <div>
                        <div className="scd-part-title">Vaccine Type</div>
                        <div className="scd-part-content">Mordena</div>
                    </div>
                </div>
                <button className="schedule-button">
                    Submit
                </button>
            </div>
        </div>
    );   
}