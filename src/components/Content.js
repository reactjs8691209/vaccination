import React from "react";
import "../css/Content.css";
import bigshield from '../images/big_shield.png';
import people from '../images/people.png';
import lock from '../images/lock.png';
import lightarrow from '../images/arrow-right.png';
import arrow from '../images/arrow-right-nor.png';

export default function Content(){
    return (
    <div id="mainContent">
        <div className="question">
            <div className="question-content">
                Why get vaccinated today?
            </div>
            <div className="question-answer">
            Magna adipiscing at elit at ornare lectus nibh lorem. Ac, sed ac lorem pellentesque vestibulum risus matti. In molestie condimentum malesuada non.
            </div>
        </div>
        <div className="benefit">
            <div className="main-benefit">
                <img src={bigshield} alt=""/>
                <div className="mb-title">Protects your immune system against viruses</div>
                <div className="mb-ct">Velit ut consectetur mauris, orci amet, faucibus.</div>
                <div className="mb-ct">Sit turpis fringilla ipsum pretium, dictum.</div>
                <div className="mb-ct">Dolor eget vel nulla lorem ac.</div>
                <div className="mb-more">
                    <div>Read More</div>
                    <img src={lightarrow} alt="" />
                </div>
            </div>
            <div className="other-benefit">
                <div className="ob">
                    <img src={people} alt=""/>
                    <div className="ob-text">Minimize the spread of the Virus</div>
                    <div className="ob-more">
                        <div >Read More</div>
                        <img src={arrow} alt=""/>
                    </div>
                </div>
                <div className="ob">
                    <img src={lock} alt=""/>
                    <div className="ob-text">Protect yourself</div>
                    <div className="ob-more">
                        <div>Read More</div>
                        <img src={arrow} alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
);   
}