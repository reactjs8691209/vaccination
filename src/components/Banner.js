import React from "react";
import "../css/Banner.css";
import anh1 from '../images/banner_img/anh1.png';
import anh2 from '../images/banner_img/anh2.png';
import anh3 from '../images/banner_img/anh3.png';
import anh4 from '../images/banner_img/anh4.png';
import anh5 from '../images/banner_img/anh5.png';
import bg from '../images/banner_img/bg.png';


export default function Banner(){
    return (
    <div id="Banner">
        <div className="banner-ct">
            <div className="banner-ct1">
                Get Vaccinated. Boost your Immune System.
            </div>
            <div className="banner-ct2">
                COVID-19 Vaccination Got Easier With, <font color="#17C2EC">Vaccination.ng</font>
            </div>
            <div className="banner-ct3">
                Vaccination.ng will help you find the nearest centre for vaccination, in all 36 states in Nigeria.
            </div>
            <div className="banner-bt">
                <button className="banner-bt1">Get Vaccine</button>
                <button className="banner-bt2">Help Centre</button>
            </div>
        </div>
        
        <div className="banner-img">
            <img className="anh1" src={anh1} alt=""/>
            <img className="anh2" src={anh2} alt=""/>
            <img className="bg" src={bg} alt=""/>
            <div className="speacialist">
                <img src={anh3} alt=""></img>
                <img src={anh4} alt=""></img>
                <img src={anh5} alt=" "></img>
                <div className="speacialistNum"><font color="white">20+</font><br/><font color="aqua">Speacialists</font></div>
            </div>
        </div>
    </div>
);   
}