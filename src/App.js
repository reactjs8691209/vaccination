import './App.css';
import Header from "./components/Header";
import Banner from "./components/Banner";
import Schedule from "./components/Schedule";
import Brand from "./components/Brand";
import FormCheck from "./components/FormCheck";
import Content from "./components/Content";
function App() {
  return (
    <div className="App">
      <Header/>
      <Banner/>
      <Schedule />
      <Brand/>
      <FormCheck/>
      <Content/>
    </div>
  );
}

export default App;
