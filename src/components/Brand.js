import React from "react";
import "../css/Brand.css";
import logo1 from '../images/logo1.png';
import logo2 from '../images/logo2.png';
import logo3 from '../images/logo3.png';
import logo4 from '../images/logo4.png';

export default function Brand(){
    return (
    <div id="brand">
        <img className="logo1" src={logo1} alt=""/>
        <img className="logo2" src={logo2} alt=""/>
        <img className="logo3" src={logo3} alt=""/>
        <img className="logo4" src={logo4} alt=""/>   
    </div>
);   
}